import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoSettingsComponentComponent } from './to-do-settings-component.component';

describe('ToDoSettingsComponentComponent', () => {
  let component: ToDoSettingsComponentComponent;
  let fixture: ComponentFixture<ToDoSettingsComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToDoSettingsComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoSettingsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
