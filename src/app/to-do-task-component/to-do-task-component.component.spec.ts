import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoTaskComponentComponent } from './to-do-task-component.component';

describe('ToDoTaskComponentComponent', () => {
  let component: ToDoTaskComponentComponent;
  let fixture: ComponentFixture<ToDoTaskComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToDoTaskComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoTaskComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
